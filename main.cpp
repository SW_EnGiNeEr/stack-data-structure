#include <iostream>
#include <string>

using namespace std;

class Stack{
      //making these variables private. Don't want  them used in other classes.
      private:
          int top; //variable used for the top of the stack
          int arr[5]; //stack is for a total of five items


      public:
          Stack () //stack function that hold instructions for stack data structure
          {
              top = -1;
              for(int i = 0; i < 5; i++) //for loop to iterate through 5 items in array
              {
                  arr[i]=0; //make sure that array starts at index 0
              }
          }

          bool isEmpty()
          {
              if(top == -1) // if stack value is -1, means that stack is empty
                return true;
              else
                return false;
          }
           bool isFull()
           {
               if (top == 4) //check for top value, elements range from 0 to 4
                  return true;
               else
                  return false;

           }

           void push(int val) // pushing value into stack
           {

               if(isFull())
               {
                   cout << "stack overflow" << endl;


               }
               else
               {
                top++; //increment top value by 1
                arr[top] = val;
               }


           }

           int pop() // remove value from stack

           {

               if (isEmpty())
               {

                   cout << "stack underflow" << endl;
                   return 0;

               }
               else
               {
                   int popValue = arr[top]; //store value at top of stack in popValue
                   arr[top] = 0;
                   top--; //decrement value
                   return popValue;
               }


           }

           int count()
           {

               return(top+1); // always return top + 1 value


           }

           int peek(int pos)
           {

               if(isEmpty())
               {
                   cout << "stack underflow" << endl;
                   return 0;
               }
               else
               {
                   return arr[pos];
               }


           }

           void change(int pos, int val) // changing value inside of stack (not fetching values)
           {
               arr[pos] = val;
               cout << "change value at location" << pos << endl;



           }

           void display()
           {

               cout << "All values in the stack are " << endl;
               for( int i = 4; i >= 0; i-- )
               {
                   cout << arr[i] << endl;
               }


           }

};


int main()
{

    Stack s1;
    int option, position, value;


    do
    {
        cout << "What operation do you want to perform? Select Option number. Enter 0 to exit. " << endl;
        cout << "1. Push " << endl;
        cout << "2. Pop " << endl;
        cout << "3. isEmpty() " << endl;
        cout << "4. isFull() " << endl;
        cout << "5. peek() " << endl;
        cout << "6. count() " << endl;
        cout << "7. change() " << endl;
        cout << "8. display() " << endl;
        cout << "9. Clear Screen " << endl << endl;

        cin >> option;
        switch(option)
        {
        case 0:
        break;
        case 1:
            cout<<"Enter item to push in the stack" <<endl;
            cin>>value;
            s1.push(value);
            break;
        case 2:
            cout << "Pop Function Called - Popped Value: " << s1.pop() << endl;
            break;
        case 3:
            if(s1.isEmpty())
                cout << "Stack is empty" << endl;
            else
                cout << "Stack is not Empty" << endl;
            break;
        case 4:
            if(s1.isFull())
                cout << "Stack is Full" << endl;
            else
                cout << "Stack is not full" << endl;
            break;
        case 5:
            cout << "Enter position of item you want to peek: " << endl;
            cin >> position;
            cout << "Peek Function Called - Value at position " << position << "is" << endl << s1.peek(position) << endl;
            break;
        case 6:
            cout << "Count Function Called - Number of items in the stack are: " << s1.count() << endl;
            break;
        case 7:
            cout << "Change Function Called - " << endl;
            cout << "Enter position of item you want to change: ";
            cin >> position;
            cout << endl;
            cout << "Enter value of item you want to change : ";
            cin >> value;
            s1.change(position, value);
            break;
        case 8:
            cout << "Display function called - " << endl;
            s1.display();
            break;
        case 9:
            system("cls");
            break;
        default:
            cout << "Enter proper option number " << endl;
        }


    }while(option != 0);

    return 0;
}
